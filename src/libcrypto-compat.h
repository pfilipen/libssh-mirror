#ifndef LIBCRYPTO_COMPAT_H
#define LIBCRYPTO_COMPAT_H

#include <openssl/opensslv.h>

#define NISTP256 "P-256"
#define NISTP384 "P-384"
#define NISTP521 "P-521"

#endif /* LIBCRYPTO_COMPAT_H */
